<?php
/*
Plugin Name:    Social Hero - Plugin
Plugin URI:        https://socialhero.ca
Description:    Custom functionality for Social Hero
Version:        1.0.0
Author:            Ryan Quang
Author URI:        https://socialhero.ca
License:        GPL-2.0+
License URI:    http://www.gnu.org/licenses/gpl-2.0.txt

This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This plugin. If not, see {URI to Plugin License}.
 */

include_once ABSPATH . 'wp-includes/pluggable.php';

if (!defined('WPINC')) {
    die;
}

// include all files within inc directory
foreach (glob(plugin_dir_path(__FILE__) . "/inc/*.php") as $function) {
    $function = basename($function);
    require_once plugin_dir_path(__FILE__) . 'inc/' . $function;
}

add_action('wp_enqueue_scripts', 'custom_enqueue_files', 11);

/**
 * Loads <list assets here>.
 */
function custom_enqueue_files()
{
    // if this is not the front page, abort.
    // if ( ! is_front_page() ) {
    //     return;
    // }

    // loads a CSS file in the head.
    wp_enqueue_style('theme-css', plugin_dir_url(__FILE__) . '/assets/css/theme.css', array(), filemtime(plugin_dir_path(__FILE__) . '/assets/css/theme.css'));

    wp_enqueue_script('owl-js', plugin_dir_url(__FILE__) . 'assets/js/owl.carousel.min.js', '', array(), true);
    /**
     * loads JS files in the footer.
     */

    wp_enqueue_script('theme-js', plugin_dir_url(__FILE__) . 'assets/js/main.min.js', array('mapbox-script'), array(''), true);
    // $current_user = wp_get_current_user();
    // if (!user_can( $current_user, 'administrator' )) {
    //     wp_enqueue_script( 'theme-js', plugin_dir_url( __FILE__ ) . 'assets/js/main.min.js', '', array(), true );
    // }

    if (is_page('roi-calculator') || is_page('traffic-calculator')) {
        wp_enqueue_script('zingchart-js', 'https://cdn.zingchart.com/zingchart.min.js', '', array(), true);
        wp_enqueue_script('roi-js', plugin_dir_url(__FILE__) . 'assets/js/roi_calculator.min.js', '', array(), true);
	}
	
	if (is_page('roi-calculator')) {
        wp_enqueue_script('roi-js', plugin_dir_url(__FILE__) . 'assets/js/roi_calculator.min.js', '', array(), true);
	}
	
	if (is_page('traffic-calculator')) {
        wp_enqueue_script('traffic-js', plugin_dir_url(__FILE__) . 'assets/js/traffic_calculator.min.js', '', array(), true);
    }
}

function sh_logo()
{
    echo file_get_contents(plugin_dir_url(__FILE__) . 'assets/images/sh-logo.svg');
}
add_shortcode('sh-logo', 'sh_logo');

// Writes to the Debug Log
if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}
