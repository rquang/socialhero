<?php

// check if the repeater field has rows of data
if( have_rows('category') ):

 	// loop through the rows of data
    while ( have_rows('category') ) : the_row(); 
    $link = get_sub_field('link', false, false);
    ?>
    <section class="service-category ct-section block-transition">
        <div class="ct-section-inner-wrap">
            <div class="service-category-text">
                <h2 class="service-category-title underline"><?php the_sub_field('title'); ?></h2>
                <div class="service-category-description">
                    <?php the_sub_field('description'); ?>
                </div>
                <?php if ($link) : ?>
                    <a class="service-category-link secondary-button" href="<?php echo get_the_permalink($link); ?>">learn more</a>
                <?php endif; ?>
            </div>
            <div class="service-category-image">
                <img class="svg-draw" src="<?php the_sub_field('image'); ?>" />
            </div>
        </div>
    </section>
    <?php
    endwhile;

else :

    // no rows found

endif;

?>