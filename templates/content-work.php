<a class="work-post block-transition" href='<?php the_permalink(); ?>'>
    <div class="work-post-image">
        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
    </div>
    <div class="work-post-meta">
        <span class="subheading random-letters"><?php the_field('company'); ?></span>
        <h3 class="work-post-title random-letters"><?php the_title(); ?></h3>
        <div class='work-post-content'>
            <p class="random-letters random-letters-event">
                <?php echo get_the_excerpt(); ?>
            </p>
        </div>
        <span class="work-post-link secondary-button">view case study</span>
    </div>
</a>