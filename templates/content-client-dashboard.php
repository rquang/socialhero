<?php
$user_id = get_current_user_id();
$logo = get_field('logo', 'user_' . $user_id);
$company = get_field('company', 'user_' . $user_id);
?>

<div id="client-dashboard-nav">
    <div class="client-dashboard-user">
        <div class="client-dashboard-logo">
            <img src="<?php echo $logo['url']; ?>" />
        </div>
        <div class="client-dashboard-company">
            <h4><?php echo $company; ?></h4>
        </div>
    </div>
    <ul class="client-dashboard-menu">
        <li>
            <a href="#" class="client-dashboard-button" data-dashboard="sh-calender">Schedule A Meeting</a>
        </li>
        <li>
            <a href="#" class="client-dashboard-button" data-dashboard="sh-analytics">Reports</a>
        </li>
        <li>
            <a href="#" class="client-dashboard-button" data-dashboard="sh-support">Support</a>
        </li>
        <li>
            <a href="#" class="client-dashboard-button">Logout</a>
        </li>
    </ul>
</div>
<div id="client-dashboard-content">
    <?php sh_get_template('/client-dashboard/content-calendly.php'); ?>
    <?php sh_get_template('/client-dashboard/content-agency-analytics.php'); ?>
</div>

<script>
    (function ($) {
        $('.client-dashboard-menu a').on('click', function () {
            var $this = $(this),
                $section =  $('#' + $this.data('dashboard'));
            if (!$this.hasClass('is--active')) {
                $('.client-dashboard-button').removeClass('is--active');
                $this.addClass('is--active');
                $section.addClass('is--active').siblings().removeClass('is--active');
            }
        });
        $('.client-dashboard-menu a').first().trigger('click');
    })(jQuery);
</script>