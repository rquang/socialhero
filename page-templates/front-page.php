<?php
/**
 * Template Name: Front Page
 * 
 * The template for front page.
 *
 * @package SocialHeroMedia
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			
			<?php
			while ( have_posts() ) :
				the_post();
			?>
				<div class="home-content">
					<?php the_content(); ?>
				</div>
			<?php
			endwhile; // End of the loop.
			?>			
			
            <?php get_template_part( 'template-parts/text-slider' ); ?>
            <?php get_template_part( 'template-parts/fp-services' ); ?>
            <?php get_template_part( 'template-parts/fp-cta' ); ?>
            <?php get_template_part( 'template-parts/fp-blog' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
