(function ($) {

    // Preloader with fallback of 2 seconds
    setTimeout(preloader, 2000);
    $(window).on("load", preloader);

    if ($('#mapbox').length) {
        mapBox();
    }

    if ($('#google-badge').length) {
        var rating = ($('#google-badge #ratingValue').html() / 5) * 100;
        $('#google-badge .star-rating-inner').css('width', rating + '%');
    }


    $('a').click(function (e) {
        if (link_is_external(this)) {
            return;
        }
        if (this.pathname === window.location.pathname) {
            return;
        }
        e.preventDefault();
        newLocation = this.href;
        $('.page-frame').removeClass('is--loaded');
        window.location = newLocation;

    });

    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });

    // $('.big-count').each(function () {
    //     var $this = $(this),
    //         $parent = $this.parent('.big-number-wrap'),
    //         counter = parseFloat($this.text());

    //     $parent.fadeOut(1);
    //     $this.prop('Counter', 0);

    //     var bigCountInterval = setInterval(function () {
    //         if ($this.hasClass('is--visible')) {
    //             count($this, $parent, counter);
    //             clearInterval(bigCountInterval);
    //         }
    //     }, 2000);
    // });

    function preloader() {
        if (!$('.page-frame').hasClass('is--loaded')) {
            $('.page-frame').addClass('is--loaded');
            init();
        }
    }

    function count(element, parent, counter) {
        parent.fadeIn();
        element.prop('Counter', 0).animate({
            Counter: counter
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                if (element.hasClass('big-percent')) {
                    element.text(Math.ceil(now));
                } else {
                    element.text(Math.ceil(now).toLocaleString('en'));
                }
            }
        });
    }

    function link_is_external(link_element) {
        return (link_element.host !== window.location.host);
    }

    function init() {
        chatBubble();
        isVisible();
        randomLetters();

        var testimonials = $('#testimonials .oxy-posts');
        testimonials.owlCarousel({
            items: 1,
            lazyLoad: false,
            loop: true,
            video: true,
            nav: true,
            responsive: {
                0: {
                    margin: 10,
                    stagePadding: 35
                },
                768: {
                    margin: 15,
                    stagePadding: 50
                },
                1120: {
                    margin: 50,
                    stagePadding: 150
                },
                1300: {
                    margin: 50,
                    stagePadding: 225
                },
                1500: {
                    margin: 50,
                    stagePadding: 350
                },
                1600: {
                    margin: 50,
                    stagePadding: 475
                }
            }
        });
        testimonials.on('changed.owl.carousel', function (event) {
            $('.owl-item').find('iframe').remove();
        });

        window.onscroll = function () {
            stickyHeader();
        };
        $("#menu-button").on('click', function () {
            $(this).toggleClass('is--active');
            $('#navigation').toggleClass('is--open');
            $('#navigation-menu').toggleClass('is--open');
        });
    }

    function randomLetters() {
        var nodes = document.querySelectorAll(".random-letters");
        for (var i = 0; i < nodes.length; i++) {
            var words = nodes[i].textContent;
            var html = "";
            for (var i2 = 0; i2 < words.length; i2++) {
                if (words[i2] == " ")
                    html += words[i2];
                else
                    html += "<span>" + words[i2] + "</span>";
            }
            nodes[i].innerHTML = html;
        }
    }

    function isVisible() {
        $.fn.visible = function (partial) {

            var $t = $(this),
                $w = $(window),
                viewTop = $w.scrollTop(),
                viewBottom = viewTop + $w.height(),
                _top = $t.offset().top,
                _bottom = _top + $t.height(),
                compareTop = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;

            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

        };

        // initial check
        $("#main-content *").each(function (i, element) {
            var el = $(element);
            if (el.visible(true)) {
                el.addClass("is--visible");
            }
        });

        $(window).scroll(function (event) {

            $("#main-content *").each(function (i, element) {
                var el = $(element);
                if (el.visible(true)) {
                    el.addClass("is--visible");
                }
            });
        });
    }

    function chatBubble() {
        var $parent = $('.chatbubble'),
            $text = $parent.find('.chatbubble-text'),
            array = [
                'Hello!',
                'Good Morning',
                'Good Afternoon',
                'Good Evening',
                'How do you do?',
                'Hi',
                'Hey',
                "How's it going?",
                'Howdy!',
            ],
            prevItem = $text.html(),
            prevPrevItem;
        setInterval(function () {
            var item = array[Math.floor(Math.random() * array.length)];
            while (item == prevItem || item == prevPrevItem) {
                item = array[Math.floor(Math.random() * array.length)];
            }
            $text.fadeOut(function () {
                $(this).html(item).fadeIn();
            });
            prevItem = item;
            prevPrevItem = prevPrevItem;
        }, 2000);
    }

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function stickyHeader() {
        // Get the header
        var header = document.getElementById("navigation");
        // Get the offset position of the navbar
        var sticky = header.offsetTop;
        if (window.pageYOffset > sticky) {
            header.classList.add("is--sticky");
        } else {
            header.classList.remove("is--sticky");
        }
    }

    function mapBox() {
        mapboxgl.accessToken = 'pk.eyJ1Ijoic29jaWFsaGVybyIsImEiOiJjanlnZGU0cHMwMjE4M2RtcXBqaXNsOTJvIn0.HpOGy1-JDITzlja6MyPx7A';
        var map = new mapboxgl.Map({
            container: 'mapbox',
            style: 'mapbox://styles/mapbox/dark-v10',
            center: [-113.469700, 53.473860], // starting position
            zoom: 15 // starting zoom
        });
        map.addControl(new mapboxgl.NavigationControl());
        map.loadImage('https://socialhero.ca/wp-content/uploads/2019/09/socialhero-shield-logo-white.png', function (error, image) {
            if (error) throw error;
            map.addImage('sh-logo', image);
            map.addLayer({
                "id": "places",
                "type": "symbol",
                "source": {
                    "type": "geojson",
                    "data": {
                        "type": "FeatureCollection",
                        "features": [{
                            "type": "Feature",
                            "properties": {
                                "description": "<p>9147 39 Ave NW<br>Edmonton, AB<br>T6E 5Y2</p><a href=\"https://www.google.com/maps/dir//9147+39+Ave+NW,+Edmonton,+AB+T6E+5Y2/@53.4962925,-113.4773204,13z/data=!4m8!4m7!1m0!1m5!1m1!1s0x53a018d40123dee9:0x59ab6c116b5b2484!2m2!1d-113.469284!2d53.4734842\" target=\"_blank\" title=\"Opens in a new window\">Directions</a>",
                                "icon": "theatre"
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [-113.469700, 53.473860]
                            }
                        }]
                    }
                },
                "layout": {
                    "icon-image": "sh-logo",
                    "icon-size": 1
                }
            });
        });
        map.on('load', function () {
            // When a click event occurs on a feature in the places layer, open a popup at the
            // location of the feature, with description HTML from its properties.
            map.on('click', 'places', function (e) {
                var coordinates = e.features[0].geometry.coordinates.slice();
                var description = e.features[0].properties.description;

                // Ensure that if the map is zoomed out such that multiple
                // copies of the feature are visible, the popup appears
                // over the copy being pointed to.
                while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                }

                new mapboxgl.Popup()
                    .setLngLat(coordinates)
                    .setHTML(description)
                    .addTo(map);
            });

            // Change the cursor to a pointer when the mouse is over the places layer.
            map.on('mouseenter', 'places', function () {
                map.getCanvas().style.cursor = 'pointer';
            });

            // Change it back to a pointer when it leaves.
            map.on('mouseleave', 'places', function () {
                map.getCanvas().style.cursor = '';
            });
        });
    }

})(jQuery);