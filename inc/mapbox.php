<?php
function mapbox_enqueue_scripts() {
    wp_register_style('mapbox-style', 'https://api.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css');
    wp_register_script('mapbox-script', 'https://api.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js');
}
add_action( 'wp_enqueue_scripts', 'mapbox_enqueue_scripts', 10 );
function mapbox() {
    wp_enqueue_style('mapbox-style');
    wp_enqueue_script('mapbox-script');
    return "<div id='mapbox'></div>";
}
add_shortcode("mapbox", "mapbox");