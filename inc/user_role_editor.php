<?php
add_filter('ure_role_additional_options', 'add_prohibit_access_to_admin_option', 10, 1);
function add_prohibit_access_to_admin_option($items)
{
    $item = URE_Role_Additional_Options::create_item('prohibit_admin_access', esc_html__('Prohibit access to admin', 'user-role-editor'), 'init', 'prohibit_access_to_admin');
    $items[$item->id] = $item;

    return $items;
}
function prohibit_access_to_admin()
{

    if (is_admin() && !wp_doing_ajax()) {
        wp_redirect(get_home_url());
    }
}

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */

function sh_login_redirect($redirect_to, $request, $user)
{
    //is there a user to check?
    if (isset($user->roles) && is_array($user->roles)) {
        //check for subscribers
        if (in_array('sh_client', $user->roles)) {
            // redirect them to another URL, in this case, the homepage
            $redirect_to = '/client-dashboard';
        }
    }

    return $redirect_to;
}
add_filter('login_redirect', 'sh_login_redirect', 10, 3);

/*
 *   Redirect users on logout to certain page
 */

 add_action('wp_logout', 'sh_redirect_after_logout');
function sh_redirect_after_logout()
{
    wp_redirect(home_url());
    exit();
}

/*
 *   Restrict non logged users to certain page
 */

add_action('template_redirect', 'sh_non_logged_redirect');
function sh_non_logged_redirect()
{
    $user = wp_get_current_user();
    if ((is_page('client-dashboard')) && !is_user_logged_in() && 
    empty(array_intersect(['sh_client','admin'], $user->roles))) {
        wp_redirect(home_url());
        die();
    }
}
