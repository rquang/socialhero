<?php 

function memcacheConnect() {
    $m = new Memcached;
    $m->addServer('localhost', 11211);
    return $m;
}

function memcacheCheck($key) {
    $m = memcacheConnect();
    $item = $m->get($key);
    if ($m->getResultCode() == Memcached::RES_SUCCESS) {
        return true;
    } else {
        return false;
    }
}

function memcacheStore($key, $object) {
    $m = memcacheConnect();
    $m->set($key, $object, 172800);
}

function memcacheGet($key) {
    $m = memcacheConnect();
    return $m->get($key);
}