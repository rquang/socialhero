<?php
/*
	http://stackoverflow.com/questions/12448134/social-share-links-with-custom-icons
	http://petragregorova.com/articles/social-share-buttons-with-custom-icons
	wp_enqueue_style('fontAwesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0', 'all');  
	use in template files:: <?php echo do_shortcode('[social_sharing]') ; ?>
*/
function social_sharing()
{
wp_enqueue_style('fontAwesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(),
'4.3.0', 'all');
return'
<span class="social-sharing-label">Share Us</span>
<div class="social-sharing-container">
    <a class="social-sharing-icon social-sharing-icon-facebook" target="_new"
        href="http://www.facebook.com/share.php?u=' . urlencode(get_the_permalink()) . '&title=' . urlencode(get_the_title()). '"><i
            class="fa fa-facebook"></i></a>
    <a class="social-sharing-icon social-sharing-icon-twitter" target="_new"
        href="https://twitter.com/intent/tweet?text='. urlencode(get_the_title()). '&url='. urlencode(get_the_permalink()) . '"><i
            class="fa fa-twitter"></i></a>
    <a class="social-sharing-icon social-sharing-icon-pinterest" target="_new"
        href="https://pinterest.com/pin/create/button/?url=' . urlencode(get_the_permalink()) . '&media=' . urlencode(get_template_directory_uri()."/img/logo.png")
        . '&description=' .
        urlencode(get_the_title()). '"><i class="fa fa-pinterest"></i></a>
	<a class="social-sharing-icon social-sharing-icon-linkedin" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url=' .
        urlencode(get_the_permalink()) . '&title=' . urlencode(get_the_title()) . '&source=' . get_bloginfo("url") . '"><i class="fa fa-linkedin"></i></a>
	<a class="social-sharing-icon social-sharing-icon-email" href="mailto:?subject=' .
        urlencode(get_the_permalink()) . '&body=Check out this article I came across ' . get_the_permalink() .'"><i
            class="fa fa-envelope"></i></a>
</div>
';
}
add_shortcode("social_sharing", "social_sharing");
?>