<?php
function aa_get_token()
{
    $user_id = get_current_user_id();
    $aa_id = get_field('agency_analytics_id', 'user_' . $user_id);

    $url = 'https://api.clientseoreport.com/v3/users/' . $aa_id . '/loginGrant';
 
    //Your username.
    $username = '';
     
    //Your password.
    $password = '5d238c22580e908c961c528f7ddf2d0a';
     
    //Initiate cURL.
    $ch = curl_init($url);
     
    //Specify the username and password using the CURLOPT_USERPWD option.
    curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);  
     
    //Tell cURL to return the output as a string instead
    //of dumping it to the browser.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     
    //Execute the cURL request.
    $result = curl_exec($ch);
     
    //Check for errors.
    if(curl_errno($ch)){
        //If an error occured, throw an Exception.
        throw new Exception(curl_error($ch));
    }

    $obj = json_decode($result);
    return $obj->data->login_url;
}
