module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-browser-sync');

    const sass = require('node-sass');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        postcss: {
            options: {
                map: false,
                processors: [
                    require('pixrem')(),
                    require('autoprefixer')({
                        browsers: 'last 2 versions'
                    }),
                    require('cssnano')()
                ],
            },
            dist: {
                expand: true,
                flatten: true,
                src: 'assets/css/theme.css',
                dest: 'assets/css/'
            }
        },
        uglify: {
            files: {
                src: 'src/js/*.js',
                dest: 'assets/js/',
                expand: true,
                flatten: true,
                ext: '.min.js'
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'src/js/*.js'],
            options: {
                jshintrc: '.jshintrc',
                expr: true,
                boss: true,
                loopfunc: true,
                sub: true,
                newcap: false,
            }
        },
        notify: {
            sass: {
                options: {
                    title: "CSS Files Compiled",
                    message: "SASS task complete"
                }
            }
        },
        sass: {
            options: {
                implementation: sass,
                sourceMap: true
            },
            dist: {
                files: {
                    "assets/css/theme.css": "src/scss/main.scss"
                }
            }
        },
        browserSync: {
            dev: {
                // bsFiles: {
                //     src: [
                //         '<%= path %>style.css',
                //         '<%= path %>js/**/*.js',
                //         '<%= path %>**/*.php'
                //     ]
                // },
                options: {
                    files: ['assets/**/*', '**/*.php'],
                    watchTask: true,
                    // debugInfoe: true,
                    // logConnections: true,
                    // notify: true,
                    proxy: "http://localhost/shm",
                    // tunnel: "external",
                    // host: '192.168.0.27'
                }
            }
        },
        watch: {
            gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['jshint'],
            },
            markup: {
                files: ['*.php', '**/*.php'],
                // options: {
                //     livereload: true,
                // }
            },
            scripts: {
                files: ['src/js/*.js'],
                tasks: ['jshint', 'uglify'],
                options: {
                    spawn: true,
                    // livereload: true,
                },
            },
            styles: {
                files: ['src/scss/**'],
                tasks: ['sass', 'notify:sass', 'postcss'],
                // tasks: ['sass', 'notify:sass', 'postcss'],
                // options: {
                //     compress: true,
                //     // livereload: true,
                // }
            },

        }
    });

    grunt.registerTask('production', ['uglify', 'jshint', 'sass', 'postcss']);
    grunt.registerTask('default', ['browserSync', 'watch']);
};